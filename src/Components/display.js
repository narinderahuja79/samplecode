import { Component, React } from "react";
import {Disp_Data, deleteData} from '../redux/actions/index';
import {connect} from 'react-redux';
import axios from 'axios';
// import Search from './SearchComponent'

class Display extends Component{
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      dataPerPage: 5,
      isPrevBtnActive: 'disabled',
      isNextBtnActive: '',

      searchString: "",
      users: props.data,
      loading: true,

      isClicked : false

    };
    // console.log('fetdata>>>>>>>>>>>>>>>>>>>', this.props.data);
  }

  componentDidMount(){
    const {Disp_Data, data}=this.props;
    // Disp_Data();
    axios.get('https://605d84019386d200171bab49.mockapi.io/abook/address/').then(res => {
            console.log('dssssssssssssss>>', res.data)
            Disp_Data(res.data);
           this.setState({loading: false}) 
        });
  }
  onCardClick = () => {
    this.setState({ isClicked : !this.state.isClicked }) 
 }
 
static getDerivedStateFromProps(nextProps, prevState) {
  if (!prevState.users.length) {
    return {
      users: nextProps.data
    };
  }
  // Return null to indicate no change to state.  
  return null;
}
  
  delete=(index)=>{
    const {deleteData}=this.props;
    // let del=JSON.parse(localStorage.getItem('empdata'));
    // del.splice(index,1);
    console.log('del>>>>>>>>>>>>>>>',index)
    deleteData(index);
  }

  handleClick = (event)=>{
    this.setState({
        currentPage:event.target.id
      });
  }

  handleNext = ()=>{
    let listid = this.state.currentPage + 1;
    this.setState({ currentPage : listid});
  }
  handlePrev = ()=>{
    let listid = this.state.currentPage - 1;
    this.setState({ currentPage : listid});
  }
  
  handleSearch = (e) => {
    let searchValue=e.target.value
    const {data}=this.props
    let serData=this.state.users;
    // console.log('search>>>>>>>>>>>>>>>>>>>',this.state.users)
    let search = searchValue.trim().toLowerCase();

    if (search.length > 0) {
        serData = serData.filter(function(user) {
        return user.FirstName.toLowerCase().match(search);
      });
      this.setState((state) => {
        state.users = serData;
        return state
      })
    }
    else{
      this.setState({users:data})
    }

  }

  render(){
    const {onEdit} = this.props;
    const { currentPage, dataPerPage, users } = this.state;
    // console.log('statdata>>>>>>>>>>>>>>>>>>>>', users)
    const indexOfLastData = currentPage * dataPerPage;
    const indexOfFirstData = indexOfLastData - dataPerPage;
    const currentdata = users.slice(indexOfFirstData, indexOfLastData);
    let renderdata;
    if (this.state.loading) {
      renderdata = <div className="mx-auto">Loading...</div>;
    } else {

    
     renderdata = currentdata.map((data, index) => {
      return(
        <tr key={index}>
          {/* <td>{data.id}</td> */}
          <td>{data.FirstName}</td>
          {/* <td>{data.emname}</td> */}
          <td>{data.LastNAme}</td>
          <td>{data.Address}</td>
          <td>{data.Contact}</td>
          <td>{data.Email}</td>
          {/* <td><button type="button" onClick={()=>this.delete(data)} className="btn btn-danger"> Delete </button> &nbsp;
              <button type="button" onClick={()=>onEdit(data)} className="btn btn-primary"> Edit </button> 
          </td> */}
          <td><span onClick={()=>this.delete(data)} className="fa fa-trash text-danger"></span> &nbsp;
              <span onClick={()=>onEdit(data)} className="fa fa-pencil-square-o text-primary"></span> 
          </td>
        </tr>
      ) 
    });
  }
     // Logic for displaying page numbers
     const pageNumbers = [];
     for (let i = 1; i <= Math.ceil(users.length / dataPerPage); i++) {
       pageNumbers.push(i);
     }
     
     const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li className="page-item page-link" style={{display:'inline'}} key={number} id={number} onClick={this.handleClick}>{number}</li>
      );
    });

    return (
      <div className="container">
        <div className="form-group has-search w-50">
            {/* <span className="fa fa-search form-control-feedback"></span> */}
            {/* <Search 
              searchData={users}
            /> */}
            <input
              type="text"
              ref="search"
              onChange={(event)=>this.handleSearch(event)}
              placeholder="type name here"
              className="form-control"
            />
          </div>
        <table className="table">
         <thead className="thead-light">
           <tr>
             {/* <th scope="col">Id</th> */}
             <th scope="col">First Name</th>
             {/* <th scope="col">Middle Nam</th> */}
             <th scope="col">Last Name</th>
             <th scope="col">Address</th>
             <th scope="col">Contact</th>
             <th scope="col">Email</th>
             <th scope="col">Action</th>
           </tr>
         </thead>
         <tbody>
          {renderdata}
        </tbody>
        </table>
        <nav aria-label="Page navigation example" style={{marginLeft: '0rem' }}>
        <ul id="page-numbers" className="pagination justify-content-center mx-auto">
          <li className="page-item page-link" onClick={this.handlePrev}>Previous</li>
            {renderPageNumbers}
          <li className="page-item page-link" id={renderPageNumbers} onClick={this.handleNext}>Next</li>  
        </ul>
        </nav>
        
      </div>
    );
}
     
}

const mapStateToProps = state => {
  const data= state.formData.data
  return {data};
}


export default connect(
  mapStateToProps,
  {Disp_Data,deleteData}
) (Display);