import React, { Component } from 'react'
import Navbar from './NavbarComponents'


class Profile extends Component{

  constructor() {
    super();
    this.state = { 
      username:'',
      contact:'',
      gender:'',
      address:'',
      email:'',
      password:'',
    };
  }
  componentDidMount(){
    let profilenData = JSON.parse(localStorage.getItem("profileObject"));
    // console.log(profilenData)
    this.setState({username:profilenData.Name})
      this.setState({contact:profilenData.Contact})
      this.setState({gender:profilenData.Gender})
      this.setState({address:profilenData.Address})
      this.setState({email:profilenData.Email})
      this.setState({password:profilenData.Password})
  }

  handleChange=(event)=>{
    this.setState({
      [event.target.name] : event.target.value
    })
    console.log("data>>>>>>>>>>>>>>>>>", this.state);
  }

  handleSubmit=()=>{
    alert('dsdsd')
    const { username,contact,gender,address,email,password} = this.state
    // console.log('555555555555555555555>>>>',this.state.email)
    let storage = JSON.parse(localStorage.getItem("regData"));
    let findEmail=storage.findIndex(emailObj=>(emailObj.Email===email))
    
    if (findEmail) {
      var formData={
        Name:username,
        Contact:contact,
        Gender:gender,
        Address:address,
        Email:email,
        Password:password,
      }
      console.log("updateData>>>>>>>>>>>>>",formData)
      storage[findEmail] = formData;
      console.log("regdata>>>>>>>>>>>>>>>>>>>>>", storage)
      // console.log(upemp);
      localStorage.setItem('regData',JSON.stringify(storage));
      localStorage.setItem('profileObject',JSON.stringify(formData));
    }
    
  }

    render(){
      let profileData = JSON.parse(localStorage.getItem("profileObject"));
      console.log('profileData>>>>>>>>>>', this.state.username)
      

      return(
        <>
          <Navbar />
          <h5 className="mt-2" align="center">Profile Update</h5>
          <hr></hr>
          <div className="container mt-5">
            <div className="row">
              <div className="col-sm ">
              <div className="card bg-light text-dark">
                  <div className="card-body text-center">
                  <table className="table ">
                      <thead>
                        <tr>
                          <th scope="col" className="w-25 ">User Name</th> : <td className="">{profileData.Name}</td>
                        </tr>
                        <tr>
                          <th scope="col" className="w-25 ">Conatct</th> : <td className="">{profileData.Contact}</td>
                        </tr>
                        <tr>
                          <th scope="col" className="w-25 ">Gedner</th> : <td className="">{profileData.Contact}</td>
                        </tr>
                        <tr>
                          <th scope="col" className="w-25 ">Address</th> : <td className="">{profileData.Address}</td>
                        </tr>
                        
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div className="col-sm">
              <div className="card bg-light text-dark">
                  <div className="card-body text-center">
                    <form>
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">User Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="username" value={this.state.username} onChange={this.handleChange} />
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label" >Contact</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="contact" value={this.state.contact} onChange={this.handleChange} />
                        </div>
                      </div>

                        <div className="form-group row">
                          <label  class="col-sm-2 col-form-label" >Gender</label><br></br>
                            
                            <div className="form-check form-check-inline">
                              <input className="form-check-input ml-3" type="radio" 
                                name="gender"
                                value="Male"
                                checked={this.state.gender === "Male"}
                                onChange={this.handleChange}
                              />                             
                              <label className="form-check-label">Male</label>
                              </div>
                        
                              <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" 
                                  name="gender"
                                  value="Female"
                                  checked={this.state.gender === "Female"}
                                  onChange={this.handleChange}
                                />
                              
                              <label className="form-check-label">Female</label>
                            </div>
                        </div>

                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control"
                            name="address"
                            value={this.state.address}
                            onChange={this.handleChange}
                            />
                        </div>
                      </div>
                      <button type="button" onClick={this.handleSubmit} class="btn btn-primary">Update</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )
    }
}

export default Profile