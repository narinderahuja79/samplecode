import React, { Component } from 'react'
import Navbar from './NavbarComponents'


class ChangePassword extends Component{

  constructor() {
    super();
    this.state={
      username:'',
      contact:'',
      gender:'',
      address:'',
      password:'',
      oldPassword:'',
      newPassword:'',
      confirmPassword:'',
      email:'',

     
      oldPasswordError:'',
      newPasswordError:'',
      confirmPasswordError:''


    }
  }

  componentDidMount(){
    let profilenData = JSON.parse(localStorage.getItem("profileObject"));
    // console.log(profilenData)
    this.setState({username:profilenData.Name})
    this.setState({contact:profilenData.Contact})
    this.setState({gender:profilenData.Gender})
    this.setState({address:profilenData.Address})
    this.setState({email:profilenData.Email})
    this.setState({password:profilenData.Password})
  }

  validation = ()=>{
    const { oldPassword,newPassword,confirmPassword, password } = this.state
    let oldPasswordError=""
    let newPasswordError=""
    let confirmPasswordError=""
    let isValid=true;

    if (!oldPassword) {
      oldPasswordError="*** please fill the old password field";
      isValid = false;
    }else if (oldPassword !== password) {
      oldPasswordError ="*** Old Passwords don't match ";
      isValid = false;
    }

    if (!newPassword) {
      newPasswordError="*** please fill the new password field";
      isValid = false;
    }

    if (!confirmPassword) {
      confirmPasswordError="*** please fill the confirm Password field";
      isValid = false;
    } else if (newPassword !== confirmPassword) {
      confirmPasswordError="*** Passwords don't match ";
      isValid = false;
    }

    this.setState({oldPasswordError})
    this.setState({newPasswordError})
    this.setState({confirmPasswordError})
    return isValid;
  }

  handleChange=(event)=>{
    this.setState({
      [event.target.name] : event.target.value
    })
  }

  handleSubmit=()=>{
    const { username,contact,gender,address,oldPassword, newPassword, confirmPassword, email} = this.state
    const isValid= this.validation();
    const chnagPassword=JSON.parse(localStorage.getItem("regData"));
    // console.log("555555555555555555>>", chnagPassword)
    
    let findEmail = chnagPassword.findIndex(emailObj=>(emailObj.Email===email))
    // console.log(">>>>>>>>>>>>", findEmail);

    if (findEmail) {     
        if (isValid) {
          var formData={
            Name:username,
            Contact:contact,
            Gender:gender,
            Address:address,
            Email:email,
            Password:newPassword,
          }
          console.log("findpass>>>>>>>>>>>>>>>>>>>", formData);
          chnagPassword[findEmail] = formData;
          localStorage.setItem('regData',JSON.stringify(chnagPassword));
          console.log("regdata>>>>>>>>>>>>>>>>>>>>>", chnagPassword)
          localStorage.setItem('profileObject',JSON.stringify(formData));  
        }
     
    }
  } 

    render(){
      return(
        <>
          <Navbar />
          <h5 className="mt-2" align="center">Change Password</h5>
          <hr></hr>
          <div className="container mt-5">
            <div className="row">
              <div className="col-sm">
              <div className="card w-75 bg-light text-dark mx-auto">
                  <div className="card-body">
                    <form>
                      <div class="form-group">
                        <label >Old Password</label>
                        <input type="password" class="form-control" placeholder="Enter Old Password"
                          name="oldPassword"
                          value = {this.state.oldPassword}
                          onChange={this.handleChange}
                          onInput={this.validation}
                        />
                        <div className="text-danger">{this.state.oldPasswordError}</div>
                      </div>

                      <div class="form-group">
                        <label >New Password</label>
                        <input type="password" class="form-control" placeholder="Enter New Password" 
                          name="newPassword"
                          value = {this.state.newPassword}
                          onChange={this.handleChange} 
                          onInput={this.validation}
                        />
                        <div className="text-danger">{this.state.newPasswordError}</div>
                      </div>

                      <div class="form-group">
                        <label >Confirm Password</label>
                        <input type="password" class="form-control" placeholder="Enter Confirm Password" 
                          name="confirmPassword"
                          value = {this.state.confirmPassword}
                          onChange={this.handleChange} 
                          onInput={this.validation}
                        />
                        <div className="text-danger">{this.state.confirmPasswordError}</div>
                      </div>
                      <button type="button" onClick={this.handleSubmit} class="btn btn-primary">Submit</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </>
      )
    }
}

export default ChangePassword