import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router'
import LoginComponents from './LoginComponents'
import Home from './Home'
import form from './form'
import Profile from './ProfileComponents'
import {Provider} from 'react-redux';
import store from '../store'
import ChangePassword from './ChangePassword'

class App extends Component{
    render(){
      return(
        <>
          <Switch>
            <Route exact path="/" component={LoginComponents}/>
            <Route exact path="/home" component={Home}/>
            <Route exact path="/profile" component={Profile}/>
            <Route exact path="/changePassword" component={ChangePassword}/>
            <Provider store={store}>
              <Route exact path="/form" component={form}/>
            </Provider>
            
            <Redirect to="/" />
          </Switch>
        </>
      )
    }
}

export default App