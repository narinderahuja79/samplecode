import {React, Component} from 'react';
import {Modal,Button} from 'react-bootstrap';

class Registration extends Component{
  constructor(props){
    super(props);
    this.state = { 
      username:'',
      contact:'',
      gender:'',
      address:'',
      email:'',
      password:'',

      usernameError:'',
      contactError:'',
      genderError:'',
      addressError:'',
      emailError:'',
      passwordError:'',

    };
    console.log("11111111111111>>>", this.props);
  }

  handleChange=(event)=>{
    this.setState({
      [event.target.name] : event.target.value
    })
    // console.log('data>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',this.state)
  }

  validation = ()=>{
    const { username,contact,gender,address,email,password} = this.state
    let usernameError=""
    let contactError=""
    let addressError=""
    let emailError=""
    let passwordError=""
    let isValid=true;

    if (!username) {
      usernameError="*** please fill the username field";
      isValid = false;
    }

    if (!contact) {
      contactError= "*** please fill the mobile field";
      isValid = false;
    }

    if (!address) {
      addressError= "*** please fill the address field";
      isValid = false;
    }

    if (!email) {
      emailError= "*** please fill the email field";
      isValid = false;
    }

    if (!password) {
      passwordError= "*** please fill the password field";
      isValid = false;
    }

    this.setState({usernameError})
    this.setState({contactError})
    this.setState({addressError})
    this.setState({emailError})
    this.setState({passwordError})
    return isValid;
  }

  handleSubmit=()=>{
    var regData = [];
    const { username,contact,gender,address,email,password} = this.state
    // console.log('555555555555555555555>>>>',this.state.email)
    const isValid= this.validation();

    let storage = JSON.parse(localStorage.getItem("regData"));

    let findEmail=storage ? storage.find(emailObj=>(emailObj.Email===email)) : null
    if(isValid){
      if (findEmail ? findEmail : null) {
        alert("Email already exits")
        console.log("findEmail>>>>>>>>>>>>>",findEmail)
      } else {
        var formData={
          Name:username,
          Contact:contact,
          Gender:gender,
          Address:address,
          Email:email,
          Password:password,
        }
        // console.log("formData>>>>>>>>>>>>>",formData)
          if(storage == null){
            regData.push(formData);
            localStorage.setItem('regData',JSON.stringify(regData));
          }
          else{
              storage.push(formData);
              localStorage.setItem('regData',JSON.stringify(storage));
          }
      }
    }
    
    
  }

  render(){
    return(
      <div className="container">
        <Modal show={this.props.isOpen} onHide={this.props.isClose}>
          <Modal.Header closeButton>
            <Modal.Title>Registration Form</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <div className="form-group">
                <label>User Name</label>
                <input type="text" className="form-control" 
                  name="username"
                  value = {this.state.username}
                  onChange={this.handleChange}
                  onInput={this.validation} 
                  placeholder="Enter User Name" />
                  <div className="text-danger">{this.state.usernameError}</div>
              </div>
              <div className="form-group">
                <label>Contact</label>
                <input type="text" className="form-control" 
                  name="contact"
                  value = {this.state.contact}
                  onChange={this.handleChange}
                  onInput={this.validation}
                  placeholder="Enter Mobile Number" />
                  <div className="text-danger">{this.state.contactError}</div>
              </div>
              <div className="form-group">
              <label>Gender</label><br></br>
                <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" 
                    name="gender"
                    value="Male"
                    checked={this.state.gender === "Male"}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">Male</label>
                  </div>
                  <div className="form-check form-check-inline">
                  <input className="form-check-input" type="radio" 
                    name="gender"
                    value="Female"
                    checked={this.state.gender === "Female"}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">Female</label>
                </div>
              </div>
              <div className="form-group">
                <label>Address</label>
                <textarea className="form-control" rows="3"
                   name="address"
                   value = {this.state.address}
                   onChange={this.handleChange}
                   onInput={this.validation}
                ></textarea>
                <div className="text-danger">{this.state.addressError}</div>
              </div>
              <div className="form-group">
                <label>Email</label>
                <input type="text" className="form-control" 
                  name="email"
                  value = {this.state.email}
                  onChange={this.handleChange}
                  onInput={this.validation}
                  placeholder="Enter Email Id" />
                  <div className="text-danger">{this.state.emailError}</div>
              </div>
              <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" 
                  name="password"
                  value = {this.state.password}
                  onChange={this.handleChange}
                  onInput={this.validation}
                  placeholder="*************" />
                  <div className="text-danger">{this.state.passwordError}</div>
              </div>
              <button type="button" onClick={this.handleSubmit} className="btn btn-primary">Submit</button>
          </form>
          </Modal.Body>
          <Modal.Footer>
            {/* <Button variant="secondary" onClick={this.props.isClose}>
              Close
            </Button> */}
            {/* <Button variant="primary" onClick={this.props.isClose}>
              Save Changes
            </Button> */}
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}


export default Registration;