import React, { Component } from 'react'
import { NavLink, Redirect } from 'react-router-dom'

class Navbar extends Component{
  constructor(){
    super();
    this.state = { 
    };
  }
  

  logout=()=>{
    alert('dsdsd')
    // let cookie1 = document.cookie="username=";
    // document.cookie=cookie1+";max-age=0";
    localStorage.removeItem('profileObject');
  }
    render(){
      
      return(
        <>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand ml-5" href="#">Address Book</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                 <NavLink className="nav-link" to="/home">Home</NavLink>
                </li>
                <li className="nav-item active">
                 <NavLink className="nav-link" to="/profile" >Profile</NavLink>
                </li>
                <li className="nav-item active">
                 <NavLink className="nav-link" to="/changePassword">ChangePassword</NavLink>
                </li>
                <li className="nav-item active">
                 <NavLink className="nav-link" to="/form">Address Book</NavLink>
                </li>
              </ul>
              <form className="form-inline my-2 my-lg-0 mr-5">
                <NavLink className="btn btn-outline-success my-2 my-sm-0" to="/" onClick={this.logout}> Logout </NavLink>
              </form>
            </div>
          </nav>
        </>
      )
    }
}

export default Navbar