import { Component, React } from "react";
import Display from './display';
import {addData, updateData} from '../redux/actions/index';
import {connect} from 'react-redux';
import axios from 'axios';
import Navbar from "./NavbarComponents";

class Form extends Component {
  constructor() {
    super();
    this.state={
      isSubmit:'',
    }
    const url="";
  }
  
  add=()=>{  
      
        const {addData} = this.props;
        var formData={
          FirstName:this.refs.fname.value,
            // emname:this.refs.mname.value,
            LastNAme:this.refs.lname.value,
            Address:this.refs.address.value,
            Contact:this.refs.contact.value,
            Email:this.refs.email.value,
        }
        addData(formData);

        this.refs.fname.value="";
        this.refs.lname.value="";
        this.refs.address.value="";
        this.refs.contact.value="";
        this.refs.email.value="";
    }
    
    
    edit=(index)=>{
      // alert(index)
      this.setState({isSubmit:index})
      this.refs.fname.value=index.FirstName; 
      this.refs.lname.value=index.LastNAme; 
      this.refs.address.value=index.Address;
      this.refs.contact.value=index.Contact;
      this.refs.email.value=index.Email;
    }

    Update=(index)=>{
      // alert(index);
      console.log('up>>>>>>>>>>>>>>>>>>', index)
      const {updateData} = this.props;
      var formData={
        id:index.id,
        FirstName:this.refs.fname.value,
        LastNAme:this.refs.lname.value,
        Address:this.refs.address.value,
        Contact:this.refs.contact.value,
        Email:this.refs.email.value,
      }
      updateData(formData);

      this.setState({isSubmit:''})
      this.refs.fname.value="";
      this.refs.lname.value="";
      this.refs.address.value="";
      this.refs.contact.value="";
      this.refs.email.value="";
    }
    
render(){
    return (
      <>
      <Navbar />
      <h5 className="mt-2" align="center">Address Book</h5>
          <hr></hr>
      <div className="container mt-3">
          <div class="card">
            <div class="card-header">
            Address Book
            </div>
            <div class="card-body">
              <form>
                <div className="form-group">
                  <label >First Name</label>
                  <input type="text" className="form-control" ref="fname" placeholder="Enter Your First Name" />
                </div>
                {/* <div className="form-group">
                  <label >Middle Name</label>
                  <input type="text" className="form-control" ref="mname"  placeholder="Enter Your Middle Name"  />
                </div> */}
                <div className="form-group">
                  <label >Last Name</label>
                  <input type="text" className="form-control" ref="lname" placeholder="Enter Your Last Name" />
                </div>
                <div className="form-group">
                  <label>Address</label>
                  <textarea className="form-control" rows="3" ref="address" ></textarea>
                </div>
                <div className="form-group">
                  <label >Contact</label>
                  <input type="text" className="form-control" ref="contact" placeholder="Enter Your Contact Number" />
                </div>
                <div className="form-group">
                  <label >Email</label>
                  <input type="text" className="form-control" ref="email" placeholder="Enter Your Email" />
                </div>
                { this.state.isSubmit ? <button type="button" onClick={()=>this.Update(this.state.isSubmit)}  className="btn btn-primary">Update</button>
                : <button type="button" onClick={this.add} className="btn btn-success">Submit</button>}
                
              </form>
            </div>
          </div>
          
          <div class="card mt-3">
            <div class="card-header">
              Display Book
            </div>
            <div class="card-body">
              <Display onEdit={this.edit}/>
            </div>
          </div>
          
      </div>
      </>
    );
  }
}

export default connect(
  null,
  {addData, updateData}
) (Form);
