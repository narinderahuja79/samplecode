import {React, Component} from 'react';
import { Redirect } from 'react-router';
import Registration from './Registration'
class Login extends Component{
  constructor(){
    super();
    this.state = { 
      isModalOpen: false,
      regemail:'',
      regpass:''
    };
  }

  handleAdd= () =>{
    // alert('hello')
    this.setState({ isModalOpen: true });
    console.log(this.state.isModalOpen)
  }

  handleClose= () =>{
    // alert('hello')
    this.setState({ isModalOpen: false });
    console.log(this.state.isModalOpen)
  }

  handleChange=(event)=>{
    console.log(event);
    this.setState({
      [event.target.name] : event.target.value
    })
    console.log('data>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',this.state)
  }

  handleLogin=()=>{
    // console.log(this.props.history)
    let loginData = JSON.parse(localStorage.getItem("regData"));
    const { regemail, regpass} = this.state
    let logintregdata =loginData ? loginData.find(loginObj => (loginObj.Email===regemail) && (loginObj.Password===regpass)) : null;
    console.log("loginData>>>>>>>>>>>>>",logintregdata);
    if(logintregdata ? logintregdata : null)
    { 
      // var Cookie = document.cookie = "username="+ regemail;
      alert("login successfully");
      // <Redirect to="/home" />
      localStorage.setItem('profileObject',JSON.stringify(logintregdata));
      this.props.history.push('/home')
      
    }
    else{
      alert("Invalid Emial Or Password !");
    }
  }

  render(){

    return(
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a className="navbar-brand ml-5" href="#">Address Book</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              {/* <li className="nav-item active">
                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
              </li> */}
            </ul>
            <form className="form-inline my-2 my-lg-0 mr-5">
              <button className="btn btn-outline-success my-2 my-sm-0" type="button" onClick={this.handleAdd} >Registration</button>
            </form>
          </div>
        </nav>

        <Registration isOpen={this.state.isModalOpen} isClose={this.handleClose} />
        
        <div className="card mt-5 w-50 mx-auto">
          <h5 className="card-header text-center">User Login</h5>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label>Email address</label>
                <input type="email" className="form-control"
                  name="regemail" 
                  value = {this.state.regemail}
                  onChange={this.handleChange} 
                  placeholder="Enter email"/>
              </div>
              <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control"
                  name="regpass"
                  value = {this.state.regpass}
                  onChange={this.handleChange} 
                  placeholder="Password" />
              </div>
              <button type="button" onClick={this.handleLogin} className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}


export default Login
