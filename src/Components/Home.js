import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Navbar from './NavbarComponents'
import axios from 'axios';


class Home extends Component{

  constructor() {
    super();
    this.state={
      totalBooks:'',
      loading: true,
    }
  }
  componentDidMount() {
    axios.get('https://605d84019386d200171bab49.mockapi.io/abook/address/')
      .then(res => {
        console.log("res3333333333333333333>>", res.data)
        const books =res.data.length;
        this.setState({totalBooks:books});
        this.setState({loading: false})
      })
  }
    render(){
      let nameData = JSON.parse(localStorage.getItem("profileObject"));
      var {totalBooks, loading}=this.state;
      // var CookieName = document.cookie;
      return(
        <>
          <Navbar /> 
          <h5 className="mt-2 ml-3">Welcome {nameData.Name}</h5>
          <hr></hr>
          <div class="container mt-5">
            <div class="row">
              <div class="col-sm">
              <div class="card w-25 bg-light text-dark">
                  <div class="card-body text-center">
                    <h4 className="mt-3 ml-4">Total Address Book</h4>
                    {loading ?
                      <p>Lodding...</p>
                      :
                      <p class="card-text"><h3>{totalBooks}</h3></p>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )
    }
}

export default Home