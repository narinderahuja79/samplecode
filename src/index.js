import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from './Components/form';
// import {Provider} from 'react-redux';
// import store from './store'
import App from './Components/app'
import {BrowserRouter} from 'react-router-dom'


ReactDOM.render(
  <React.StrictMode>

    {/* <Provider store={store}>
      <Form />,
    </Provider> */}
    <BrowserRouter>
      <App />
    </BrowserRouter>

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
